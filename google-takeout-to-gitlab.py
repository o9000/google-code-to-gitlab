#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 o9000
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gitlab
import json
import re
import requests
import urllib

def niceColor(name):
    if name.startswith("Status: -"):
        return "#cfcfcf"
    if name == "Status: New":
        return "#428bca"
    if name == "Status: Accepted":
        return "#5cb85c"
    if name == "Status: NeedInfo":
        return "#f0ad4e"
    if name == "Status: Started":
        return "#8e44ad"
    if name == "Status: Started":
        return "#8e44ad"
    if name == "Status: Wishlist":
        return "#a8d695"
    #
    if name == "Priority: Critical":
        return "#ffcfcf"
    if name == "Priority: High":
        return "#deffcf"
    if name == "Priority: Medium":
        return "#fff5cc"
    if name == "Priority: Low":
        return "#cfe9ff"
    #
    if name == "Type: Defect":
        return "#d9534f"
    if name == "Type: Enhancement":
        return "#44ad8e"
    if name == "Type: Other":
        return "#7f8c8d"
    if name == "Type: Review":
        return "#8e44ad"
    if name == "Type: Task":
        return "#4b6dd0"
    #
    if name.startswith("Component:"):
        return "#fff39e"
    #
    if name.startswith("OpSys:"):
        return "#e2e2e2"
    #
    if name.startswith("Milestone:"):
        return "#fee3ff"
    return "#e2e2e2"


def niceName(name):
    return name.replace("-", ": ", 1)


def niceStatusName(name, open):
    return "Status: " + ("-" if not open else "") + name


def niceDate(date):
    return date.replace("T", " ").replace(".000Z", "")


def niceAuthor(author):
    parts = author.split("@", 1)
    parts[0] = parts[0][:-3] + "..."
    return "@".join(parts)


def niceIssue(id):
    return "[issue %s](./%s)" % (id, id)


def linkifyIssues(s):
    return re.sub(r'([Ii]ssue) ([0-9]+)', r'\1 #\2', s)


def escapeForMarkdown(s):
    s = s.replace("*", "\\*")
    s = s.replace("#", "\\#")
    s = s.replace("`", "\\`")
    s = s.replace(":", "\\:")
    s = s.replace("-", "\\-")
    s = s.replace("+", "\\+")
    s = s.replace("_", "\\_")
    s = s.replace("(", "\\(")
    s = s.replace(")", "\\)")
    s = s.replace("[", "\\[")
    s = s.replace("]", "\\]")
    s = s.replace("<", "\\<")
    s = s.replace(">", "\\>")
    s = s.replace("\r", "")
    s = s.replace("\n", "  \n")
    return s


def labelExists(glp, name):
    for label in glp.Label():
        if label.name == name:
            return True
    return False


def createLabel(gl, glp, name):
    color = niceColor(name)
    gitlab.ProjectLabel(gl, {"project_id": glp.id, "name": name, "color": color}).save()


def ensureLabelExists(gl, glp, name):
    if not labelExists(glp, name):
        print "Creating label \"%s\"" % name
        createLabel(gl, glp, name)


def niceAttachments(comment, projectName, issueId, commentId):
    attachmentLines = []
    if "attachments" in comment:
        for attachment in comment["attachments"]:
            if "isDeleted" in attachment:
                line = "*Attachment: [%s] (deleted)*" % (attachment["fileName"])
                attachmentLines.append(line)
                continue
            link = "https://storage.googleapis.com/google-code-attachments/%s/issue-%d/comment-%d/%s" % (projectName, issueId, max(0, commentId-1), attachment["fileName"])
            #link1 = "https://storage.googleapis.com/google-code-attachments/%s/issue-%d/comment-%d/%s" % (projectName, issueId, max(0, commentId-1), attachment["fileName"])
            #link2 = "https://storage.googleapis.com/google-code-attachments/%s/issue-%d/comment-%d/%s" % (projectName, issueId, max(0, commentId), attachment["fileName"])
            #link3 = "https://storage.googleapis.com/google-code-attachments/%s/issue-%d/comment-%d/%s" % (projectName, issueId, max(0, commentId+1), attachment["fileName"])
            if urllib.urlopen(link).getcode() != 200:
                print "Bad attachment link: %s for issue %d, comment %d, attachment %s" % (link, issueId, commentId, json.dumps(attachment))
            else:
                pass
                #print "Good link: %s for issue %d, comment %d, attachment %s" % (link, issueId, commentId, json.dumps(attachment))
            line = "*Attachment: [%s](%s)*" % (attachment["fileName"], link)
            attachmentLines.append(line)
    return "\n\n".join(attachmentLines)


#######################


print "This is a Google Code -> GitLab project migration tool."
print ""
print "The program loads a GoogleCodeProjectHosting.json file from the current directory and recreates",
print "all the Google Code projects and their issue trackers on a GitLab server."
print ""
print "For each Google Code project:"
print "* Creates a new project on GitLab with the same name and summary (unless it already exists)."
print "* Exports all the predefined issue status labels to GitLab (only the name; the label description is not exported)."
print "* Exports all the predefined issue labels to GitLab (only the name; the label description is not exported)."
print "* Exports all the custom issue labels to GitLab."
print "* For each issue, exports:"
print "  * The title (summary);"
print "  * The author and date as text;"
print "  * The labels;"
print "  * The state (open/closed);"
print "  * For each comment, exports:"
print "    * The author and date as text;"
print "    * The text;"
print "    * The status updates (label, status, owner and merge changes) as text;"
print "    * Creates links to the original attachments stored on Google Cloud Storage, but does not upload them to GitLab."
print "  * Issues are exported unless they already exist. If they exist, the comments, title, labels and the state are updated."
print "* Does NOT export the code, the wiki, the users or other content (main page, icon, links, downloads)."
print ""
print "Notes:"
print "* The program has been built for a one-time migration, not keeping projects in sync. However syncing may be partially successful."
print "* In case of malfunctions, the GitLab projects may become a mess. Some actions are irreversible (e.g.",
print "it is impossible to delete issues on GitLab), so use this with great care and on a dedicated account with no existing projects."
print "* License: GNU Lesser General Public License version 3, i.e. offers no warranty and is provided \"as is\"."
print ""

dryRun = raw_input("Dry run? [Y/n]: ").lower() != "n"

if raw_input("Start exporting? [y/N]: ").lower() != "y":
    exit(-1)

with open("GoogleCodeProjectHosting.json") as f:
    gc = json.load(f)

if raw_input("SSL verification might be broken (at least when executed in Python 2.7.3). Continue anyways? [y/N]: ").lower() != "y":
    exit(-1)

defaultUrl = "https://gitlab.com"
remoteUrl = raw_input("Please enter the server URL [%s]: " % defaultUrl)
if not remoteUrl:
    remoteUrl = defaultUrl

token = raw_input("Please enter your private token (you can find it in %s/profile/account): " % remoteUrl)

fillEmptyLabels = raw_input("Fill missing predefined labels with Label: X? [y/N]: ").lower() == "y"

gl = gitlab.Gitlab(remoteUrl, token)
gl.auth()

print "Logged in as user %s" % gl.user.username

print "Turning off further SSL warnings."
requests.packages.urllib3.disable_warnings()

for gcp in gc["projects"]:
    print "Found Google Code project:", gcp["name"]
    glp = None
    for p in gl.Project(None):
        if p.name == gcp["name"]:
            glp = p
            break
    if not glp:
        print "Creating GitLab project %s" % gcp["name"]
        if not dryRun:
          glp = gitlab.Project(gl, {
              "name": gcp["name"],
              "description": gcp["summary"]
          })
          glp.save()
    else:
        print "GitLab project %s already exists." % gcp["name"]
    print "Searching for existing issues..."
    existingIssues = {}
    page = 1
    while True:
        issues = glp.Issue(None, page=page)
        page = page + 1
        if not issues:
            break
        for issue in issues:
            existingIssues[issue.iid] = issue
    # Predefined labels
    print "Exporting predefined status labels..."
    knownLabels = set()
    statusOpenness = {}
    for status in gcp["issuesConfig"]["statuses"]:
        name = niceStatusName(status["status"], status["meansOpen"])
        print "Label:", name
        statusOpenness[status["status"]] = status["meansOpen"]
        if not dryRun:
            ensureLabelExists(gl, glp, name)
        knownLabels.add(name)
    print "Exporting predefined labels..."
    columns = set([])
    if fillEmptyLabels:
        for label in gcp["issuesConfig"]["labels"]:
            name = niceName(label["label"])
            print "Label:", name
            if not dryRun:
                ensureLabelExists(gl, glp, name)
            knownLabels.add(name)
            if ":" in name:
                columns.add(name.split(":", 1)[0])
    for column in columns:
        if not dryRun:
            ensureLabelExists(gl, glp, column + ": X")
    # Issues
    print "Exporting issues..."
    lastId = 0
    for issue in gcp["issues"]["items"] if "items" in gcp["issues"] else []:
        while issue["id"] > lastId + 1:
            lastId = lastId + 1
            if lastId not in existingIssues:
                if not dryRun:
                    newIssue = gitlab.ProjectIssue(gl, {
                        "project_id": glp.id,
                        "title": "Blank",
                        "description": "This issue has been deleted."
                    })
                    newIssue.save()
                    newIssue.state_event = "close"
                    newIssue.save()
                    newIssue = None
        lastId = issue["id"]
        print "Exporting issue %d of %d..." % (issue["id"], gcp["issues"]["items"][-1]["id"])
        for label in issue["labels"]:
            name = niceName(label)
            if name not in knownLabels:
                if not dryRun:
                    ensureLabelExists(gl, glp, name)
                knownLabels.add(name)
        comments = issue["comments"]["items"]
        newIssue = None
        if issue["id"] in existingIssues:
            newIssue = existingIssues[issue["id"]]
            print "Updating existing issue %s" % issue["id"]
            labels = [niceName(label) for label in issue["labels"]]
            labels.append(niceStatusName(issue["status"], statusOpenness[issue["status"]]))
            for column in columns:
                found = False
                for label in labels:
                    if ":" in label:
                        labelColumn = label.split(":", 1)[0]
                        if labelColumn == column:
                            found = True
                            break
                if not found:
                    labels.append("%s: X" % column)
            if set(labels) != set(newIssue.labels):
                print "Updating labels:", labels
                if not dryRun:
                    newIssue.labels = labels
                    newIssue.save(labels=labels)
            if issue["title"] != newIssue.title:
                print "Updating title:", labels
                if not dryRun:
                    newIssue.save(title=issue["title"])
        if not newIssue:
            print "Creating issue..."
            author = niceAuthor(issue["author"]["name"])
            authorLink = issue["author"]["htmlLink"]
            date = niceDate(issue["published"])
            importLine = "*Issue %d by [%s](%s) on %s:*\n\n" % (issue["id"], author, authorLink, date)
            attachments = niceAttachments(comments[0], gcp["name"], issue["id"], 0)
            body = linkifyIssues(escapeForMarkdown(comments[0]["content"]))
            if not body:
                body = "*(No comment has been entered for this change)*"
            if attachments:
                body = body + "\n\n" + attachments
            if not dryRun:
                labels = [niceName(label) for label in issue["labels"]]
                labels.append(niceStatusName(issue["status"], statusOpenness[issue["status"]]))
                for column in columns:
                    found = False
                    for label in labels:
                        if ":" in label:
                            labelColumn = name.split(":", 1)[0]
                            if labelColumn == column:
                                found = True
                                break
                    if not found:
                        labels.append("%s: X" % column)
                newIssue = gitlab.ProjectIssue(gl, {
                    "project_id": glp.id,
                    "title": issue["title"],
                    "description": importLine + body,
                    "labels": labels
                })
                newIssue.save()
        # Export comments
        if not dryRun:
            existingComments = []
            commentPage = 1
            while True:
                commentSubset = newIssue.Note(None, page=commentPage)
                commentPage = commentPage + 1
                if not commentSubset:
                    break
                for comment in commentSubset:
                    existingComments.append(comment)
        else:
            existingComments = []
        for comment in comments[1 + len(existingComments):]:
            if "deletedBy" in comment:
                continue
            print "Creating comment %d..." % comment["id"]
            author = niceAuthor(comment["author"]["name"])
            authorLink = comment["author"]["htmlLink"]
            date = niceDate(comment["published"])
            labelLine = ""
            if "labels" in comment["updates"]:
                labelLine = "Labels: " + ", ".join([(("~~" + l[1:] + "~~") if l.startswith("-") else l) for l in comment["updates"]["labels"]])
            if labelLine:
                labelLine = "*" + labelLine + "*\n\n"
            statusLine = ""
            if "status" in comment["updates"]:
                statusLine = "Status: " + comment["updates"]["status"]
            if statusLine:
                statusLine = "*" + statusLine + "*\n\n"
            ownerLine = ""
            if "owner" in comment["updates"]:
                ownerLine = "Owner: " + comment["updates"]["owner"]
            if ownerLine:
                ownerLine = "*" + ownerLine + "*\n\n"
            mergedLine = ""
            if "mergedInto" in comment["updates"]:
                mergedLine = "Merged into: %s" % niceIssue(comment["updates"]["mergedInto"])
            if mergedLine:
                mergedLine = "*" + mergedLine + "*\n\n"
            importLine = "*Comment by [%s](%s) on %s:*\n\n" % (author, authorLink, date)
            attachments = niceAttachments(comment, gcp["name"], issue["id"], comment["id"])
            body = linkifyIssues(escapeForMarkdown(comment["content"]))
            if not body:
                body = "*(No comment has been entered for this change)*"
            if attachments:
                body = body + "\n\n" + attachments
            if not dryRun:
                newComment = gitlab.ProjectIssueNote(gl, {
                    "project_id": glp.id,
                    "issue_id": newIssue.id,
                    "body": importLine + statusLine + labelLine + ownerLine + mergedLine + body
                })
                newComment.save()
        # Save state
        if issue["state"] == "closed" and newIssue.state != "closed":
            if not dryRun:
                print "Marking issue %d as closed..." % issue["id"]
                newIssue.state_event = "close"
                newIssue.save()
