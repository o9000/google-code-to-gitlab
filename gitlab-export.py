#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gitlab
import json
import re
import requests
import urllib

print "This script exports the GitLab project data into file GitLabProjectHosting.json with a format similar to the "
print "GoogleCodeProjectHosting.json generated for Google Code."
print ""
print "For each GitLab project:"
print "* Exports the name, the labels, the list of members and the issues."
print "* For each issue, exports:"
print "  * The title (summary);"
print "  * The author, creation date and modification date;"
print "  * The labels;"
print "  * The state (open/closed);"
print "  * The owner;"
print "  * The comments."
print "  * For each comment, exports:"
print "    * The author and date as text;"
print "    * The text;"
print "    * Does NOT export status updates (label, status, owner and merge changes);"
print "    * Does NOT export attachments."
print "* Does NOT export the code, the wiki or other content (icon, links, downloads)."
print ""
print "Notes:"
print "* License: GNU Lesser General Public License version 3, i.e. offers no warranty and is provided \"as is\"."
print ""

if raw_input("Start exporting? [y/N]: ").lower() != "y":
    exit(-1)

if raw_input("SSL verification might be broken (at least when executed in Python 2.7.3). Continue anyways? [y/N]: ").lower() != "y":
    exit(-1)

defaultUrl = "https://gitlab.com"
remoteUrl = raw_input("Please enter the server URL [%s]: " % defaultUrl)
if not remoteUrl:
    remoteUrl = defaultUrl

token = raw_input("Please enter your private token (you can find it in %s/profile/account): " % remoteUrl)

gl = gitlab.Gitlab(remoteUrl, token)
gl.auth()

print "Logged in as user %s" % gl.user.username

print "Turning off further SSL warnings."
requests.packages.urllib3.disable_warnings()

exported = {"kind" : "projecthosting#user", "id" : "1", "projects": []}

for glp in gl.Project(None):
    print "Project:", glp.name
    project = {
        "kind" : "projecthosting#project",
        "name" : glp.name,
        "externalId" : glp.name,
        "htmlLink" : glp.web_url,
        "summary" : glp.description,
        "description" : "",
        "versionControlSystem" : "git",
        "repositoryUrls" : [ glp.http_url_to_repo, glp.ssh_url_to_repo ],
        "role" : "owner",
        "members" : [],
        "issueConfig": {},
        "issues": {
            "kind" : "projecthosting#issueList",
            "totalResults" : 0,
            "items" : []
        }
    }
    page = 1
    while True:
        members = glp.Member(None, page=page)
        page = page + 1
        if not members:
            break
        for member in members:
            print "Member:", member.username
            project["members"].append({
                "kind" : "projecthosting#issuePerson",
                "name" : member.username,
                "displayName" : member.name,
                "htmlLink" : "https://gitlab.com/%s/" % member.username
            })
    project["issueConfig"] = {
        "kind" : "projecthosting#projectIssueConfig",
        "defaultColumns" : [ "ID", "Owner", "Summary" ],
        "defaultSorting" : [ "-ID" ],
        "statuses" : [ {
              "status" : "Open",
              "meansOpen" : True,
              "description" : "Open issue"
            }, {
              "status" : "Closed",
              "meansOpen" : False,
              "description" : "Closed issue"
            } ],
        "labels" : [],
        "prompts" : [ {
          "name" : "Defect report from user",
          "title" : "Enter one-line summary",
          "description" : "What steps will reproduce the problem?\n1.\n2.\n3.\n\nWhat is the expected output? What do you see instead?\n\n\nWhat version of the product are you using? On what operating system?\n\n\nWhich window manager (e.g. openbox, xfwm, metacity, mutter, kwin) or\nwhich desktop environment (e.g. Gnome 2, Gnome 3, LXDE, XFCE, KDE)\nare you using?\n\n\nPlease provide any additional information below. It might be helpful\nto attach your tint2rc file (usually located at ~/.config/tint2/tint2rc).",
          "titleMustBeEdited" : True,
          "status" : "New",
          "labels" : [ ],
          "defaultToMember" : True
        } ],
        "defaultPromptForMembers" : 0,
        "defaultPromptForNonMembers" : 0,
        "usersCanSetLabels" : False
    }
    labels = glp.Label(None, page=page)
    for label in labels:
        print "Label:", label.name
        project["issueConfig"]["labels"].append({
            "label" : label.name,
            "color" : label.color,
            "description" : label.name
        })
    issuePage = 1
    while True:
        issues = glp.Issue(None, page=issuePage)
        issuePage = issuePage + 1
        if not issues:
            break
        for issue in issues:
            print "Issue", issue.iid
            exportedIssue = {
                "kind" : "projecthosting#issue",
                "id" : issue.iid,
                "title" : issue.title,
                "summary" : issue.title,
                "stars" : 1,
                "starred" : True,
                "status" : issue.state,
                "state" : issue.state,
                "labels" : issue.labels if not issue.milestone else issue.labels + ["Milestone: " + issue.milestone.title],
                "author" : {
                  "kind" : "projecthosting#issuePerson",
                  "name" : issue.author.username,
                  "htmlLink" : "https://gitlab.com/%s/" % issue.author.username
                },
                "updated" : issue.updated_at,
                "published" : issue.created_at,
                "projectId" : glp.name,
                "canComment" : True,
                "canEdit" : True,
                "comments" : {
                    "kind" : "projecthosting#issueCommentList",
                    "totalResults" : 0,
                    "items" : []
                }
            }
            if issue.assignee:
                exportedIssue["owner"] = {
                    "kind" : "projecthosting#issuePerson",
                    "name" : issue.assignee,
                    "htmlLink" : "https://gitlab.com/%s/" % issue.assignee
                }
            commentPage = 1
            comments = []
            while True:
                commentSubset = issue.Note(None, page=commentPage)
                commentPage = commentPage + 1
                if not commentSubset:
                    break
                for comment in commentSubset:
                    comments.append(comment)
            comments.sort(key=lambda x: x.created_at)
            for commentIndex in range(1 + len(comments)):
                print "Comment", commentIndex
                comment = None if commentIndex == 0 else comments[commentIndex-1]
                exportedIssue["comments"]["items"].append({
                    "id" : commentIndex,
                    "kind" : "projecthosting#issueComment",
                    "author" : {
                      "kind" : "projecthosting#issuePerson",
                      "name" : comment.author.username if comment else issue.author.username,
                      "htmlLink" : "https://gitlab.com/%s/" % (comment.author.username if comment else issue.author.username)
                    },
                    "content" : comment.body if comment else issue.description,
                    "published" : comment.created_at if comment else issue.created_at,
                    "updates" : {
                      "kind" : "projecthosting#issueCommentUpdate"
                      # TODO
                    },
                    "canDelete" : True
                })
                exportedIssue["comments"]["totalResults"] = exportedIssue["comments"]["totalResults"] + 1
            project["issues"]["totalResults"] = project["issues"]["totalResults"] + 1
            project["issues"]["items"].append(exportedIssue)
    exported["projects"].append(project)

with open("GitLabProjectHosting.json", "w") as f:
    json.dump(exported, f, indent=4, separators=(',', ': '))
