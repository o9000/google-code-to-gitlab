## Use the official GitLab migration tool instead

GitLab is going to release soon a tool to import Google Code projects
(see https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/471 ).

No further improvements are going to be made to these scripts.

## Google Code to GitLab migration tool

This is a Google Code -> GitLab project migration tool.

The program loads a GoogleCodeProjectHosting.json file from the current directory and recreates all
the Google Code projects and their issue trackers on a GitLab server.

For each Google Code project:
* Creates a new project on GitLab with the same name and summary (unless it already exists).
* Exports all the predefined issue status labels to GitLab (only the name; the label description is not exported).
* Exports all the predefined issue labels to GitLab (only the name; the label description is not exported).
* Exports all the custom issue labels to GitLab.
* For each issue, exports:
  * The title (summary);
  * The author and date as text;
  * The labels;
  * The state (open/closed);
  * For each comment, exports:
     * The author and date as text;
     * The text;
     * The status updates (label, status, owner and merge changes) as text;
     * Does NOT export attachments.
  * Issues are exported unless they already exist. If they exist, only the comments and the state are updated.
* Does NOT export the code, the wiki, the users or other content (main page, icon, links, downloads).

## Notes

* The program has been built for a one-time migration, not keeping projects in sync.
* In case of malfunctions, the GitLab projects may become a mess. Some actions are irreversible (e.g. it is impossible
to delete issues on GitLab), so use this with great care and on a dedicated account with no existing projects.
* License: GNU Lesser General Public License version 3, i.e. offers no warranty and is provided "as is".

## How to use

1. Download your issue tracker data using [Google Takeout](https://www.google.com/settings/takeout).

2. Extract the archive and navigate to the directory containing the json file. Then run:
```
./google-takeout-to-gitlab.py
```

## Credits

Based on https://github.com/gpocentek/python-gitlab.git