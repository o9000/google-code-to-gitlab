#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 o9000
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import getopt
import json
import re
import sys

def crawl(object, callback):
    callback(object)
    if isinstance(object, dict):
        keys = object.keys()
        for key in keys:
            crawl(object[key], callback)
    elif isinstance(object, list):
        for item in object:
            crawl(item, callback)


def mask(str):
    return str[:-3] + "..."


def maskEmail(author):
    parts = author.split("@", 1)
    parts[0] = mask(parts[0])
    if len(parts) == 2:
      parts[1] = mask(parts[1][0:6])
    return "@".join(parts)


def sanitize(object):
    if isinstance(object, dict) and "kind" in object and object["kind"] == "projecthosting#issuePerson":
        keys = object.keys()
        for key in keys:
            if key == "name":
                object["name"] = maskEmail(object["name"])
            elif key == "kind":
                pass
            else:
                del object[key]
    elif isinstance(object, dict) and "kind" in object and object["kind"] == "projecthosting#user":
        if "id" in object:
            del object["id"]
    elif isinstance(object, dict) and "kind" in object and object["kind"] == "projecthosting#issueCommentUpdate":
        if "cc" in object:
            object["cc"] = [maskEmail(s) for s in object["cc"]]


def usage():
    print "{} -i <input> -o <output>".format(sys.argv[0])


#####################

input = ""
output = ""
try:
    opts, args = getopt.getopt(sys.argv[1:], "hi:o:", ["input=", "output="])
except getopt.GetoptError:
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == "-h":
       usage()
       exit(0)
    elif opt in ("-i", "--input"):
       input = arg
    elif opt in ("-o", "--output"):
       output = arg
if not input or not output:
    usage()
    exit(1)

with open(input) as f:
    takeout = json.load(f)

crawl(takeout, sanitize)

with open(output, "w") as f:
    json.dump(takeout, f, indent=1, separators=(',', ': '))
