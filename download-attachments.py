#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 o9000
# Copyright (C) 2015 Sven Strickroth <email@cs-ware.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
import BeautifulSoup
import json
import re
import requests
import time
import os.path


def downloadAttachments(projectName, issueId, errorFileName):
    issuepath = "{}/{}".format(projectName, issueId)
    if not os.path.exists(issuepath):
        os.mkdir(issuepath)
    print "Extracting attachments for issue {}".format(issueId)
    url = 'https://code.google.com/p/{}/issues/detail?id={}'.format(projectName, issueId)
    r = requests.get(url)
    links = []
    if r.status_code == 200:
        try:
            soup = BeautifulSoup.BeautifulSoup(r.content)
            links = list(set([a.get('href') for a in soup.findAll('a', attrs={'href': re.compile(r"^//{}\.googlecode\.com/issues/attachment\?".format(projectName))})]))
        except:
            print "Could not parse page {}".format(url)
    else:
        print "Could not download page {}".format(url)
        return {}
    result = {}
    links = [('https:' + link) if link.startswith('//') else link for link in links]
    patternId = re.compile('aid=([^&]+)')
    for link in links:
        matches = patternId.findall(link)
        if len(matches) == 1:
            attachmentId = matches[0]
            if attachmentId in result:
                continue
            cachepath = "{}/{}".format(issuepath, attachmentId)
            if os.path.isfile(cachepath + ".bin"):
                print "Already downloaded: attachment ID {}".format(attachmentId)
                with open(cachepath + ".bin", 'rb') as content_file:
                    content = content_file.read()
                result[attachmentId] = base64.b64encode(content)
                continue
            r = requests.get(link, cookies=r.cookies)
            if r.status_code == 200:
                print "Found attachment ID {} of {} bytes".format(attachmentId, len(r.content))
                result[attachmentId] = base64.b64encode(r.content)
                with open(cachepath + ".txt", "w") as myfile:
                    myfile.write(r.headers['content-disposition'])
                with open(cachepath + ".bin", "wb") as myfile:
                    myfile.write(r.content)
            else:
                logError(errorFileName, "Could not download attachment {} from {}!".format(attachmentId, link))
    return result


def logError(errorFileName, errorString):
    print errorString
    if errorFileName != "":
        with open(errorFileName, "a") as myfile:
            myfile.write(errorString + "\n")


#####################


with open("GoogleCodeProjectHosting.json") as f:
    takeout = json.load(f)

if raw_input("SSL verification might be broken (at least when executed in Python 2.7.3). Continue anyways? [y/N]: ").lower() != "y":
    exit(-1)

delay = raw_input("Add a random delay between issue downloads of [10] seconds (to prevent an IP ban): ")
if delay == "":
    delay = 10
else:
    delay = int(delay)

errorFileName = "errors.txt"

requests.get('https://code.google.com/')
print "Turning off further SSL warnings."
requests.packages.urllib3.disable_warnings()

for project in takeout["projects"]:
    print "Found Google Code project:", project["name"]
    if not os.path.exists(project["name"]):
        os.mkdir(project["name"])
    if "items" in project["issues"]:
        for issue in project["issues"]["items"]:
            if "deletedBy" in issue:
                continue
            # possible optimization: check to only start downloading if a issue contains non deleted attachments
            hasAttachments = any(["attachments" in comment for comment in issue["comments"]["items"]])
            if hasAttachments:
                time.sleep(delay)
                downloads = downloadAttachments(project["name"], issue["id"], errorFileName)
                for comment in issue["comments"]["items"]:
                    if "deletedBy" in comment:
                        continue
                    for attachment in comment["attachments"] if "attachments" in comment else []:
                        if "isDeleted" in attachment:
                            continue
                        if attachment["attachmentId"] in downloads:
                            attachment["content"] = downloads[attachment["attachmentId"]]
                        else:
                            logError(errorFileName, "No attachment downloaded for attachment id {}, issue id {}, project {}".format(attachment["attachmentId"], issue["id"], project["name"]))

print "Saving GoogleCodeProjectHostingComplete.json ..."
with open("GoogleCodeProjectHostingComplete.json", "w") as f:
    json.dump(takeout, f, indent=1, separators=(',', ': '))
